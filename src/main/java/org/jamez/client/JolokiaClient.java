package org.jamez.client;

import org.jolokia.client.*;
import org.jolokia.client.request.*;

import java.util.Map;

public class JolokiaClient {

    //  http://localhost:9090/testapp/jolokia/exec/ch.qos.logback.classic:Name=testapp,Type=ch.qos.logback.classic.jmx.JMXConfigurator/getLoggerLevel/org.jamez
    //  http://localhost:9090/testapp/jolokia/exec/ch.qos.logback.classic:Name=testapp,Type=ch.qos.logback.classic.jmx.JMXConfigurator/setLoggerLevel/org.jamez/ERROR

    public static void main(String[] args) throws Exception {

        getLogLevel();
        setLogLevel("DEBUG");
        getLogLevel();

        System.out.println("Before: " + getGreeter());
        setGreeter("1324");
        System.out.println("After: " + getGreeter());
//        memoryUsage();
    }



    private static String getLogLevel() throws Exception {
        J4pClient j4pClient = new J4pClient("http://localhost:9090/testapp/jolokia");
        J4pExecRequest req = new J4pExecRequest("ch.qos.logback.classic:Name=testapp,Type=ch.qos.logback.classic.jmx.JMXConfigurator",
                "getLoggerLevel", "org.jamez");
        J4pExecResponse resp = j4pClient.execute(req);
        String vals = resp.getValue();
        System.out.println("Vals: " + vals);
        return vals;
    }

    private static void setLogLevel(String level) throws Exception {
        J4pClient j4pClient = new J4pClient("http://localhost:9090/testapp/jolokia");
        J4pExecRequest req = new J4pExecRequest("ch.qos.logback.classic:Name=testapp,Type=ch.qos.logback.classic.jmx.JMXConfigurator",
                "setLoggerLevel", "org.jamez", level);
        J4pExecResponse resp = j4pClient.execute(req);
    }

    private static String getGreeter() throws Exception {
        J4pClient j4pClient = new J4pClient("http://localhost:9090/testapp/jolokia");
        J4pReadRequest req = new J4pReadRequest("jamezSpringBootTestApp:name=HelloController",
                "Greeter");
        J4pReadResponse resp = j4pClient.execute(req);
        String vals = resp.getValue();
        System.out.println("Vals: " + vals);
        return vals;
    }

    private static String setGreeter(String value) throws Exception {
        J4pClient j4pClient = new J4pClient("http://localhost:9090/testapp/jolokia");
        J4pWriteRequest writeReq = new J4pWriteRequest("jamezSpringBootTestApp:name=HelloController",
                "Greeter", value);
        J4pWriteResponse writeResp = j4pClient.execute(writeReq);
        String writeRespVal = writeResp.getValue();
        return writeRespVal;
    }

    private static void memoryUsage() throws Exception {
        J4pClient j4pClient = new J4pClient("http://localhost:9090/testapp/jolokia");
        J4pReadRequest req = new J4pReadRequest("java.lang:type=Memory",
                "HeapMemoryUsage");
        J4pReadResponse resp = j4pClient.execute(req);
        Map<String, Long> vals = resp.getValue();
        long used = vals.get("used");
        long max = vals.get("max");
        int usage = (int) (used * 100 / max);
        System.out.println("Memory usage: used: " + used +
                " / max: " + max + " = " + usage + "%");
    }
}