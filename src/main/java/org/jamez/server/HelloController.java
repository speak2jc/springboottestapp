package org.jamez.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@ManagedResource(objectName = "jamezSpringBootTestApp:name=HelloController")
public class HelloController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    private String greeter = "Spring Boot";

    @RequestMapping("/")
    public String index() {
        LOGGER.debug("About to generate greeting");
        String greeting = "Greetings from " + greeter + "!";
        LOGGER.warn("I couldn't think of a really good greeting - please improve");
        LOGGER.debug("Generated greeting");
        return greeting;
    }

    @ManagedAttribute
    public String getGreeter() {
        return greeter;
    }

    @ManagedAttribute
    public void setGreeter(String greeter) {
        this.greeter = greeter;
    }

    @ManagedOperation
    public void logGreeterValue(String requester){
        System.out.println(requester + " requested to log JmxManagedValue which is: " + greeter);
        LOGGER.info(requester + " requested to log JmxManagedValue which is: " + greeter);
    }
}
